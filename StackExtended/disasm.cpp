#include <iostream>
#include <assert.h>

void disasm ();

int main ()
{
    disasm ();
    return 0;
}

void disasm ()
{
    FILE *file = fopen ("testasm", "rb");
    assert (file);

    int *cmd = (int *) calloc (1024, sizeof (int));
    int count = 0;
    while (!feof (file))
    {
        fread (& (cmd[count]), sizeof (int), 1, file);
        count++;
    }

    for (int i = 0; i < count; i++)
    {
        printf ("%d\n", cmd[i]);
    }
    free (cmd);
}