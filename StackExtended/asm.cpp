#include <iostream>
#include <assert.h>
#include <cstring>

int  read_file (FILE *file, char *buffer);
int  parse_string (const char *string, char **comands);
int  convert_charcmd_to_int (const char **commands, int count, int *intCmd);
void asm_start ();

int main ()
{
    asm_start();
    return 0;
}

void asm_start ()
{
    FILE *file = fopen ("testpars", "r");
    assert (file);

    char *buffer = (char *) malloc (1024 * sizeof (char));
    assert (buffer);

    int readRes = read_file (file, buffer);
    assert (!readRes);
    fclose (file);

    char **commands = (char **) malloc (1024 * sizeof (char *));
    assert (commands);

    int count = parse_string (buffer, commands);
    assert (count != -1);
    assert (count != -2);

    int *cmdInt = (int *) malloc (1024 * sizeof (int));
    convert_charcmd_to_int((const char **) commands, count, cmdInt);

    file = fopen ("testasm", "wb");
    assert (file);

    fwrite (cmdInt, sizeof (int), count, file);

    //free (buffer); // FIXME
    for (int i = 0; i < count; i++) free(commands[i]);
    free (commands);
    free (cmdInt);
    fclose (file);
}


int read_file (FILE *file, char *buffer)
{
    //assert (file);
    if (!file)   return -1;
    //assert (buffer);
    if (!buffer) return -2;

    fseek (file, 0, SEEK_END);
    long lSize = ftell (file);
    rewind (file);
    size_t number = fread (buffer, sizeof (char), lSize, file);

    assert (number == lSize);

    return 0;
}

int parse_string (const char *string, char **commands)
{
    //assert (string);
    if (!string)   return -1;
    //assert (commands);
    if (!commands) return -2;

    char *istr = (char *) malloc (sizeof (char) * 1024);
    assert (istr);
    char *tmp =  (char *) malloc (strlen (string) * sizeof (char));
    assert (tmp);

    strcpy (tmp, string);
    istr = strtok (tmp, " \n");
    int count = 0;
    for (int i = 0; istr != NULL; i++, count++)
    {
        size_t len = strlen (istr);
        commands[i] = (char *) malloc (len + sizeof (char));
        assert (commands[i]);
        strcpy (commands[i], istr);
        istr = strtok (NULL, " \n");
    }
    free (istr);
    free (tmp);
    return count;
}

int convert_charcmd_to_int (const char **commands, int count, int *intCmd)
{
    //assert (commands);
    if (!commands) return -1;
    //assert (intCmd);
    if (!intCmd)   return -2;

    char *tmpCmd = (char *) malloc (1024 * sizeof (char));
    assert (tmpCmd);
    for (int i = 0; i < count; i++)
    {
        strcpy (tmpCmd, commands[i]);

        #define STACK_CMD(name, num, code) \
        if (strcmp (tmpCmd, name) == 0)    \
        {                                  \
            intCmd[i] = num;               \
            continue;                      \
        }

        #include "CmdList.h"

        #undef STACK_CMD

        intCmd[i] = atoi (commands[i]);

    }
    free (tmpCmd);
    return 0;
}