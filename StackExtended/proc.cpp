#include <iostream>
#include <assert.h>
#include "stack.h"
#include "register.h"

void proc ();
int  read_cmd (int *cmd, FILE *file);
void calculate (int *cmd, int count, _Stack<int> *stack, _Stack<int> *stackReturn);
void print_result (_Stack<int> *stack);

int main ()
{
    proc ();
    return 0;
}

void proc ()
{
    FILE *file  = fopen ("testasm", "rb");
    assert (file);

    _Stack<int> stack       = {};
    _Stack<int> stackReturn = {};
    //Registers registers     = {};

    int *cmd = (int *) calloc (1024, sizeof (int));
    assert (cmd);
    int readRes = read_cmd(cmd, file);
    fclose (file);

    calculate (cmd, readRes, &stack, &stackReturn);

    print_result (&stack);

    free (cmd);
}

int read_cmd (int *cmd, FILE *file)
{
    assert (cmd);
    assert (file);

    int count = 0;

    while (!feof (file))
    {
        fread (& (cmd[count]), sizeof (int), 1, file);
        count++;
    }
    count--;

    return count;
}

void calculate (int *cmd, int count, _Stack<int> *stack, _Stack<int> *stackReturn)
{
    for (int i = 0; i < count; ++i)
    {
        switch (cmd[i])
        {
            #define STACK_CMD(name, num, code)\
            {case num: code break;}

            #include "CmdList.h"

            default:
                fprintf (stderr, "Unknown commands\n");
                abort();

            #undef STACK_CMD
        }

    }
}

void print_result (_Stack<int> *stack)
{
    while (stack->count > 0)
    {
        printf ("%d\n", stack->pop ());
    }
}