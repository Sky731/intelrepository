#ifndef STACKEXTENDED_REGISTER_H
#define STACKEXTENDED_REGISTER_H

typedef struct _registers
{
    int ax = 0;
    int bx = 0;
    int cx = 0;
    int dx = 0;
    int r[16];

    _registers()
    {
        for (int i = 0; i < 16; ++i) {
            r[i] = 0;
        }
    }
} Registers;

#endif //STACKEXTENDED_REGISTER_H
