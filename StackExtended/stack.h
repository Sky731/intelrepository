#ifndef STACK_STACK_H
#define STACK_STACK_H

#include <cstdlib>
#include <cassert>
#include <iostream>

#define DEADF00D -666666

template <typename T>
struct _Stack
{
    int count = 0;
    int allocated = 0;
    T *bottom = NULL;

    void push (T x)
    {
        assert (this);
        if (this->bottom == NULL)
        {
            this->bottom = (T *) malloc (sizeof (T)); // malloc to calloc
            this->allocated++;
        }
        if (this->count == this->allocated)
        {
            int doublesize = 2 * this->allocated;
            this->bottom = (T *) realloc (this->bottom, doublesize * sizeof (T));
            this->allocated = doublesize;
        }
        this->bottom[this->count] = x;
        this->count++;
    }

    T pop ()
    {
        assert (this);

        if (this->count <= 0) return DEADF00D;

        this->count--;
        T result = this->bottom[this->count];
        if (this->allocated > 2 * this->count)
        {
            this->bottom = (T *) realloc (this->bottom, this->allocated / 2 * sizeof (T));
            this->allocated /= 2;
        }
        return result;
    }

    void clean ()
    {
        assert (this);

        this->count;
        this->count = 0;
        this->allocated = 0;
        free (this->bottom);
        this->bottom = NULL;
    }

    T add ()
    {
        assert (this);
        T x = pop () + pop ();
        push (x);
        return x;
    }

    T sub ()
    {
        assert (this);
        T bot = pop ();
        T top = pop ();
        T x = top - bot;
        push (x);
        return x;
    }

    T mul ()
    {
        assert (this);
        T x = pop () * pop ();
        push (x);
        return x;
    }

    T div ()
    {
        assert (this);
        T bot = pop ();
        T top = pop ();
        T x = top / bot;
        push (x);
        return x;
    }

    T in ()
    {
        assert (this);
        T x = 0;
        std::cin >> x;
        this->push (x);
        return x;
    }

    T out ()
    {
        assert (this);
        T x = pop ();
        std::cout << x;
        return x;
    }
};


#endif //STACK_STACK_H
