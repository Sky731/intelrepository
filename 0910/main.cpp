#include <cstdio>
#include <cstdlib>

#define SQR(x) ({ auto _x = (x); _x * _x; })

#ifndef NDEBUG
#define ASSERT(var)                                     \
({                                                      \
    auto _x = (var);                                    \
    if (!_x) {                                          \
    fprintf (stderr, "all gone bad with \"" #var "\""); \
    abort ();                                           \
    }                                                   \
})
#else
#define ASSERT(var) ;
#endif

int main()
{
    int sqr5 = SQR(5);
    printf ("%d\n", sqr5);
    ASSERT(sqr5 == 25);
    return 0;
}