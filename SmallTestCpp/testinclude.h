#ifndef SMALLTESTCPP_TESTINCLUDE_H
#define SMALLTESTCPP_TESTINCLUDE_H

template <typename T>
struct t
{
    T c;
};

template <typename T>
void tSqr (t<T>* x);
template <typename T>
void tSqr (t<T>* x)
{
    x->c = x->c * x->c;
}

#endif //SMALLTESTCPP_TESTINCLUDE_H
