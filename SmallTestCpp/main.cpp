#include <iostream>
#include "testinclude.h"

using namespace std;

int main() {
    t<int> x = {};
    x.c = 4;
    cout << x.c << endl;
    tSqr (&x);
    cout << x.c << endl;

    return 0;
}