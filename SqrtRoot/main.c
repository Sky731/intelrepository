#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

int findRoot (double a, double b, double c, double *x1, double *x2);

int main ()
{
    double a, b, c;
    int countRead;
    countRead = scanf ("%lf%lf%lf", &a, &b, &c);
    assert (countRead == 3);
    double x1, x2;
    int rootnum = findRoot (a, b, c, &x1, &x2);
    switch (rootnum)
    {
        case 0:
            printf ("Root isn't founded\n");
            break;
        case 1:
            printf ("Found 1 root:\n%lf", x1);
            break;
        case 2:
            printf ("Found 2 roots:\n%lf\t%lf", x1, x2);
            break;
        case -1:
            printf ("Found infinitely roots\n");
            break;
        case -2:
            printf ("It is not an equation\n");
            break;
    }
    return 0;
}
// Возвращает кол-во корней, если -1 -> бесконечное кол-во корней.
// Остальные отрицательные значения - коды ошибок
int findRoot (double a, double b, double c, double *x1, double *x2)
{
    if (a != 0 && b != 0 && c != 0)
    {
        double discriminant = b*b - 4*a*c;
        if (discriminant > 0)
        {
            (*x1) = (-b + sqrt (discriminant)) / (2*a);
            (*x2) = (-b - sqrt (discriminant)) / (2*a);
            return 2;
        }
        if (discriminant == 0)
        {
            (*x1) = -b / (2*a);
            return 1;
        }
        if (discriminant < 0) return 0;
    }
    if (a == 0 && b != 0 && c != 0)
    {
        (*x1) = -c / b;
        return 1;
    }
    if (a != 0 && b == 0 && c != 0)
    {
        if ((-c / a) <= 0) return 0;
        (*x1) = sqrt (-c / a);
        (*x2) = -sqrt (-c / a);
        return 2;
    }
    if (a != 0 && b != 0 && c == 0)
    {
        (*x1) = 0;
        (*x2) = -b / a;
        return 2;
    }
    if (a != 0 || b != 0)
    {
        (*x1) = 0;
        return 1;
    }
    if (c != 0) return 0;
    else return -1;
    return -2; // Неверные данные
}

/*int isZero (double x)
{
    const int dx = 0.0001;
    return (abs (x) < dx) ? 1 : 0;
}*/
