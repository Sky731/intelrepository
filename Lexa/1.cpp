#include <stdio.h>
#include <stdlib.h>

int ReadArray (int *arr);
void PrintArray (int *arr, int size);
void swap (int *arr, int size);

int main ()
{
    int arr[1024];
    int size = ReadArray (arr);
    swap (arr, size);
    PrintArray (arr, size);
    return 0;
}

void PrintArray (int *arr, int size)
{
    int i = 0;
    for (i = 0; i < size; ++i)
    {
        printf ("%d ", arr[i]);
    }
    printf ("\n");
}

int ReadArray (int *arr)
{
    int size = 0;
    printf ("Vvedite kolvo elementov: ");
    scanf ("%d", &size);
    int i = 0;
    printf ("Vvedite elementbl: ");
    for (i = 0; i < size; ++i) {
        scanf ("%d", &arr[i]);
    }
    return size;
}

void swap (int *arr, int size)
{
    int i = 0;
    int flag = 0;
    int min = 0;
    int max = 0;
    for (i = 0; i < size; ++i)
    {
        if (arr[i] % 2 == 0 && arr[i] > arr[max])
        {
            max = i;
            flag++;
        }
        else if (arr[i] % 2 != 0 && arr[i] < arr[min])
        {
            min = i;
            flag++;
        }
    }
    if (flag > 1)
    {
        int tmp = arr[min];
        arr[min] = arr[max];
        arr[max] = tmp;
    }
}


