#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int* Sum (const int a[], const int b[], const int size);
int Input (int a[], int size);
int Print (const int a[], int size);

int main ()
{
    const int size = 10;
    int a[size], b[size];
    Input (a, size);
    Input (b, size);
    int *result = Sum (a, b, size);
    Print (result, size);
    return 0;
}

int* Sum (const int a[], const int b[], const int size)
{
    assert (a);
    assert (b);
    int* res = (int*)calloc(size, sizeof(res[0]));
    assert (res);
    //static int res[10];
    int i;
    for (i = 0; i < size; i++)
    {
        assert (0 <= i && i < size);
        res[i] = a[i] + b[i];
    }
    return res;
}

int Input (int a[], int size)
{
    int i;
    for (i = 0; i < size; i++)
    {
        scanf ("%d", &a[i]);
    }
    return 0;
}

int Print (const int a[], int size)
{
    int i;
    for (i = 0; i < size; i++)
    {
        printf ("%d ", a[i]);
    }
    return 0;
}
