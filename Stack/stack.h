#ifndef STACK_STACK_H
#define STACK_STACK_H

#include <cstdlib>
#include <cassert>

#define DEADF00D -666666

template <typename T>
struct _Stack
{
    int count = 0;
    int allocated = 0;
    T *bottom = NULL;
};

template <typename T>
void push (_Stack<T> *stack, T x);

template <typename T>
T pop     (_Stack<T> *stack);

template <typename T>
int clean (_Stack<T> *stack);

template <typename T>
void push (_Stack<T> *stack, T x)
{
    assert (stack);
    if (stack->bottom == NULL)
    {
        stack->bottom = (T *) malloc (sizeof (T)); // malloc to calloc
        stack->allocated++;
    }
    if (stack->count == stack->allocated)
    {
        int doublesize = 2 * stack->allocated;
        stack->bottom = (T *) realloc (stack->bottom, doublesize * sizeof (T));
        stack->allocated = doublesize;
    }
    stack->bottom[stack->count] = x;
    stack->count++;
}

template <typename T>
T pop (_Stack<T> *stack)
{
    assert (stack);

    if (stack->count <= 0) return DEADF00D;

    stack->count--;
    T result = stack->bottom[stack->count];
    if (stack->allocated > 2 * stack->count)
    {
        stack->bottom = (T *) realloc (stack->bottom, stack->allocated / 2 * sizeof (T));
        stack->allocated /= 2;
    }
    return result;
}

template <typename T>
int clean (_Stack<T> *stack)
{
    assert (stack);

    int resCount = stack->count;
    stack->count = 0;
    stack->allocated = 0;
    free (stack->bottom);
    stack->bottom = NULL;

    return resCount;
}


#endif //STACK_STACK_H
