#include <stdlib.h>
#include "SkyString.h"

int SkyStrlen (const char *string)
{
	if (string == NULL) return -1; // NULL exeption
	int i;
	for (i = 0; *string != 0; string++, i++)
	{}
	return i;
}

char* SkyStrcat (char* destptr, const char* srcptr)
{
	// NULL exeption
	if (destptr == NULL || srcptr == NULL) return NULL;
	//FIXME bag if destptr == srcptr
	if (destptr == srcptr) return NULL; // Link is equals
	int lensrc = SkyStrlen (srcptr);
	destptr = realloc (destptr, (lensrc - 1) * sizeof (char));
	int lendest = SkyStrlen (destptr);
	int i;
	for (i = 0; i <= lensrc; i++)
	{
		destptr[lendest + i] = srcptr[i];
	}
	return destptr;
}

char* SkyStrstr (const char* sBig, const char* sSmall)
{
	if (sBig == NULL || sSmall == NULL) return NULL;
	int i;
	const char* zerosBig = sBig;
	const char* zerosSmall = sSmall;
	for (i = 0; *sBig != 0; sBig++, i++)
	{
		if (*sBig == *sSmall)
		{
			char* result = sBig;
			for (; *sSmall != 0; sSmall++, sBig++)
			{
				if (*sBig == 0) return NULL;
				if (*sSmall == *sBig) continue;
				break;
			}
			if (*sSmall == 0) return result;
			sSmall = zerosSmall;
		}
	}
	return NULL;
}

 //complete this
long int SkyStrtoul (const char* string, char** end)
{
	if (string == NULL) return -1;
	for (; *string != 0 && !(*string >= '0' && *string <= '9'); string++) 
	{}
	long int result = 0;
	for (int i = 0; string[i] != 0 && string[i] >= '0'
					&& string[i] <= '9'; i++)
	{
		result *= 10;
		result += (int) string[i] - (int) '0';
	}
	if (end != NULL)
	{
		*end = string;
	}
	return result;
}

