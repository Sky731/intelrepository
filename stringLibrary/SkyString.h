#ifndef _SKYSTRING_H_
#define _SKYSTRING_H_

int SkyStrlen (const char *string);
char* SkyStrcat (char* destptr, const char* srcptr);
char* SkyStrstr (const char* sBig, const char* sSmall);
long int SkyStrtoul (const char* string, char** end);


#endif
