#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

int  readFile_and_parsingString (FILE *f, char **result);
int  rhymeGenerate (char **arr, size_t size);
int  strcompare (void *s1, void *s2);
int  isRhyme (char *s1, char *s2);
int  unitTestOnegin ();

int main ()
{
	int resultTesting = unitTestOnegin ();
	if (resultTesting == 0)
	{
		printf ("=========================\n"
				"Testing successfully completed!\n"
				"=========================\n");
	}
	else
	{
		printf ("=========================\n"
				"Testing failed with error code: %d =(\n%s", resultTesting,
				"=========================\n");
	}
	return 0;
}

int unitTestOnegin ()
{
	FILE *myfile = fopen ("poem.txt", "r");
	assert (myfile != NULL);
	char **buf = calloc (1, sizeof (char *));
	int arrSize = readFile_and_parsingString (myfile, buf);
	fclose (myfile);
	char **alfabet = calloc (arrSize, sizeof (char *));
	for (int i = 0; i < arrSize; i++)
	{
		alfabet[i] = buf[i];
	}
	qsort (alfabet, arrSize - 1, sizeof (char *), strcompare);
	for (int k = 0; k < arrSize; k++)
	{
		printf ("%s\n", alfabet[k]);
	}
	printf ("\n=================================\n");
	rhymeGenerate (alfabet, arrSize);
	for (int k = 0; k < arrSize; k++)
	{
		printf ("%s\n", alfabet[k]); // don't know, what is bag. Do not write to a file
									 // sorry, my English is so bad =)
	}
	return 0;
}

int rhymeGenerate (char **arr, size_t size)
{
	assert (arr != NULL);
	for (int i = 0; i < size - 4; i += 4) //FIXME
	{
		for (int j = size - 1; j > i; j--)
		{
			if (isRhyme (arr[i], arr[j]))
			{
				char *tmp = arr[i + 2];
				arr[i + 2] = arr[j];
				arr[j] = tmp;
				break;
			}
		}
		for (int j = size - 1; j > i; j--)
		{
			if (isRhyme (arr[i + 1], arr[j]))
			{
				char *tmp = arr[i + 3];
				arr[i + 3] = arr[j];
				arr[j] = tmp;
				break;
			}
		}
	}
	return 0;
}

int isRhyme (char* s1, char *s2) // can be improved
{
	assert (s1 != NULL);
	assert (s2 != NULL);
	size_t len1 = strlen (s1);
	size_t len2 = strlen (s2);
	while (s1[len1 - 1] == '!' || s1[len1 - 1] == '.'  || s1[len1 - 1] == ',' ||
		   s1[len1 - 1] == ';' || s1[len1 - 1] == '\"' || s1[len1 - 1] == '?' || s1[len1 - 1] == ' ')
	{
		len1--;
		if (len1 < 1) return 0;
	}
	while (s2[len2 - 1] == '!' || s2[len2 - 1] == '.'  || s2[len2 - 1] == ',' ||
		   s2[len2 - 1] == ';' || s2[len1 - 1] == '\"' || s2[len2 - 1] == '?' || s2[len2 - 1] == ' ')
	{
		len2--;
		if (len2 < 1) return 0;
	}

	if (s1[len1 - 1] == s2[len2 - 1]) return 1;
	else return 0;
}

int readFile_and_parsingString (FILE *f, char **result)
{
	assert (f != NULL);
	fseek (f, 0, SEEK_END);
	long lSize = ftell(f);
	rewind (f);
	char *str = calloc (lSize + 1, sizeof (char));
	assert (str != NULL);
	size_t number = fread(str, 1, lSize, f);
	assert (number == lSize);
	char *istr = calloc (1024, sizeof (char));
	istr = strtok (str, "\n");
	int i = 0;
	while (istr != NULL)
	{
		size_t len = strlen (istr);
		result[i] = malloc (len + 1);
		strcpy (result[i], istr);
		istr = strtok (NULL, "\n");
		i++;
	}
	free (istr);
	return i;
}

int strcompare (void *s1, void *s2)
{
	char *par1 = *(char**)s1;
	char *par2 = *(char**)s2;
	int res = strcmp (par1, par2);
	if (res > 0) 		return 1;
	else if (res < 0) 	return -1;
	else 				return 0;
}
