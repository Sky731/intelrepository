//
// Created by sky on 22.02.16.
//
#include <iostream>
#include "CFuncMonitor.h"

CFuncMonitor::CFuncMonitor (const char *file, int line, const char *func) :
                            file_ (file), line_ (line), func_ (func)
{
    std::cout << "Call function : " << file_ << " "
              << line_ << " " << func_ << std::endl;
}

CFuncMonitor::~CFuncMonitor ()
{
    std::cout << "Return function : " << file_ << " "
              << line_ << " " << func_ << std::endl;
}