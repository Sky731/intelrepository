#ifndef STACK_CVECTOR_H
#define STACK_CVECTOR_H

#include "CFuncMonitor.h"
#include <assert.h>

    class CVector
    {
    private:
        int begin_;
        void begend (CVector* a);
    public:
        double x;
        double y;

        CVector ();
        CVector (double x, double y);
        explicit CVector (const CVector& that);

        int ok ();

        CVector& operator+ (const CVector& a, const CVector& b);

        CVector& operator+= (CVector& a, const CVector& b);

        CVector& operator- (const CVector& a, const CVector& b);

        CVector& operator-= (CVector& a, const CVector& b);

        CVector& operator* (const CVector& a, double b);

        bool operator== (const CVector& a, const CVector& b);

        double operator^ (const CVector& a, const CVector& b); // Скалярное произведение

    private:
        int end_;
    };




#endif //STACK_CVECTOR_H
