//
// Created by sky on 22.02.16.
//

#ifndef STACK_CFUNCMONITOR_H
#define STACK_CFUNCMONITOR_H

#define $ CFuncMonitor(__FILE__, __LINE__, __PRETTY_FUNCTION__);

class CFuncMonitor
{
public:
    const char *func_;
    const char *file_;
    int line_;

    CFuncMonitor (const char *file, int line, const char *func);
    ~CFuncMonitor ();
};


#endif //STACK_CFUNCMONITOR_H
