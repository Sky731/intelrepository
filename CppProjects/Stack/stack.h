#ifndef STACK_STACK_H
#define STACK_STACK_H

#include <cstdlib>
#include <cassert>
#include <iostream>

#define DEADF00D -666666

template <typename T>
class CStack
{
private:
    int count_;
    int allocated_;
    T *bottom_;

public:

    void push (T x);

    T pop     ();

    int clean ();

    CStack    ();

    ~CStack   ();

    int dump  ();

    void push_back (T x);
};

template <typename T>
CStack<T>::CStack () : count_ (0), allocated_ (0), bottom_ (NULL)
{
    bottom_ = (T *) malloc (sizeof (T)); // malloc to calloc
    allocated_ = 1;
}

template <typename T>
CStack<T>::~CStack ()
{
    free (bottom_);
    bottom_ = NULL;
    count_ = 0;
    allocated_ = 0;
}

template <typename T>
void CStack<T>::push (T x)
{
    assert (this);

    if (!bottom_)
    {
        bottom_ = (T *) malloc (sizeof (T)); // malloc to calloc
        allocated_ = 1;
    }

    if (count_ == allocated_)
    {
        int doublesize = 2 * allocated_;
        bottom_ = (T *) realloc (bottom_, doublesize * sizeof (T));
        allocated_ = doublesize;
    }
    bottom_[count_] = x;
    count_++;
}

template <typename T>
T CStack<T>::pop ()
{
    assert (this);

    if (count_ <= 0) return DEADF00D;

    count_--;
    T result = bottom_[count_];
    if (allocated_ > 2 * count_)
    {
        bottom_ = (T *) realloc (bottom_, allocated_ / 2 * sizeof (T));
        allocated_ /= 2;
    }

    return result;
}

template <typename T>
void CStack<T>::push_back (T x)
{

}

template <typename T>
int CStack<T>::clean ()
{
    assert (this);

    int resCount = count_;
    count_ = 0;
    allocated_ = 0;
    free (bottom_);
    bottom_ = NULL;

    return resCount;
}

template <typename T>
int CStack<T>::dump ()
{
    assert (this);

    std::cout << "In stack "  << count_ << " elements" << std::endl;
    std::cout << "Allocated memory for " << allocated_ << " elements" << std::endl;
    std::cout << "Bottom pointer: " << bottom_ << std::endl;
    std::cout << "The stack elements:" << std::endl;

    for (int i = 0; i < count_; ++i) {
        std::cout << i << " : " << bottom_[i] << std::endl;
    }

    std::cout << "-------------------------" << std::endl;
    std::cout << "Dump done!" << std::endl;

    return 0;
}


#endif //STACK_STACK_H
