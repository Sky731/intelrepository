#include <stdlib.h>
#include "CVector.h"

void CVector::begend (CVector* a)
{
    assert (a->ok ());
    int res = rand ();
    a->begin_  = res;
    a->end_    = res;
}

int CVector::ok ()
{
    int res = 1;
    if (this == NULL || begin_ != end_) res = 0;
    return res;
}

CVector::CVector () : x(0), y(0)
{
    begend (this);
}

explicit CVector::CVector (const CVector& that)
{
    assert (that.ok ());
    x = that.x;
    y = that.y;
    begend (this);
}

CVector::CVector (double _x, double _y) : x(_x), y(_y)
{
    begend (this);
}

CVector& CVector::operator* (const CVector &a, double b)
{
    assert (a.ok());
    return CVector (a.x * b, a.y * b); //FIXME
}

CVector& CVector::operator+ (const CVector &a, const CVector &b)
{
    assert (a.ok ());
    assert (b.ok ());
    return CVector (a.x + b.x, a.y + b.y); //FIXME
}

CVector& CVector::operator+= (CVector &a, const CVector &b)
{
    assert (a.ok ());
    assert (b.ok ());
    a.x += b.x;
    a.y += b.y;
    return a;
}

CVector& CVector::operator- (const CVector &a, const CVector &b)
{
    assert (a.ok ());
    assert (b.ok ());
    return CVector (a.x - b.x, a.y - b.y); //FIXME
}

CVector& CVector::operator-= (CVector &a, const CVector &b)
{
    assert (a.ok ());
    assert (b.ok ());
    a.x -= b.x;
    a.y -= b.y;
    return a;
}

double CVector::operator^ (const CVector &a, const CVector &b)
{
    assert (a.ok ());
    assert (b.ok ());
    return a.x * b.x + a.y * b.y;
}

bool CVector::operator== (const CVector &a, const CVector &b)
{
    assert (a.ok ());
    assert (b.ok ());
    return a.x == b.x && a.y == b.x;
}